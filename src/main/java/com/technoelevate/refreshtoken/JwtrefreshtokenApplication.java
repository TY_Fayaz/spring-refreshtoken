package com.technoelevate.refreshtoken;

import java.util.ArrayList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.technoelevate.refreshtoken.entity.Role;
import com.technoelevate.refreshtoken.entity.User;
import com.technoelevate.refreshtoken.service.UserService;

@SpringBootApplication
public class JwtrefreshtokenApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtrefreshtokenApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.saveRole(new Role(null, "ROLE_USER"));
			userService.saveRole(new Role(null, "ROLE_MANAGER"));
			userService.saveRole(new Role(null, "ROLE_ADMIN"));
			userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

			userService.saveUser(new User(null, "Shaik Fayaz", "fayaz", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Shaik Ajash", "ajash", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Shaik Faizan", "faizan", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Shaik Affan", "affan", "1234", new ArrayList<>()));

			userService.addRoleToUser("fayaz", "ROLE_USER");
			userService.addRoleToUser("fayaz", "ROLE_MANAGER");
			userService.addRoleToUser("ajash", "ROLE_MANAGER");
			userService.addRoleToUser("faizan", "ROLE_ADMIN");
			userService.addRoleToUser("affan", "ROLE_SUPER_ADMIN");
			userService.addRoleToUser("ajash", "ROLE_ADMIN");
			userService.addRoleToUser("ajash", "ROLE_USER");

		};
	}

}
