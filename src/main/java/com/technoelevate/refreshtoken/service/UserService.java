package com.technoelevate.refreshtoken.service;

import java.util.List;

import com.technoelevate.refreshtoken.entity.Role;
import com.technoelevate.refreshtoken.entity.User;

public interface UserService {

	User saveUser(User user);

	Role saveRole(Role role);

	void addRoleToUser(String username, String rolename);

	User getUser(String username);

	List<User> getUsers();
}
